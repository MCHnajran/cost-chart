// Copyright (c) 2021, mariam and contributors
// For license information, please see license.txt

frappe.ui.form.on('Project Cost Scheme', {
	// refresh: function(frm) {

	// }
    total_direct:function (doc, dt, dn) {
        var totalCost = 0;
        if(!isNaN(cur_frm.doc.total_direct)){
            totalCost += cur_frm.doc.total_direct
        }
        if(!isNaN(cur_frm.doc.total_manfacture)){
            totalCost += cur_frm.doc.total_manfacture
        }
        if(!isNaN(cur_frm.doc.total_installation)){
            totalCost += cur_frm.doc.total_installation
        }
        if(!isNaN(cur_frm.doc.total_labor_cost_and_indirect_costs)){
            totalCost += cur_frm.doc.total_labor_cost_and_indirect_costs
        }
        cur_frm.set_value("total_cost", totalCost);
        refresh_field("total_cost");
    },
    total_manfacture:function (doc, dt, dn) {
        var totalCost = 0;
        if(!isNaN(cur_frm.doc.total_direct)){
            totalCost += cur_frm.doc.total_direct
        }
        if(!isNaN(cur_frm.doc.total_manfacture)){
            totalCost += cur_frm.doc.total_manfacture
        }
        if(!isNaN(cur_frm.doc.total_installation)){
            totalCost += cur_frm.doc.total_installation
        }
        if(!isNaN(cur_frm.doc.total_labor_cost_and_indirect_costs)){
            totalCost += cur_frm.doc.total_labor_cost_and_indirect_costs
        }
        cur_frm.set_value("total_cost", totalCost);
        refresh_field("total_cost");
    },
    total_installation:function (doc, dt, dn) {
        var totalCost = 0;
        if(!isNaN(cur_frm.doc.total_direct)){
            totalCost += cur_frm.doc.total_direct
        }
        if(!isNaN(cur_frm.doc.total_manfacture)){
            totalCost += cur_frm.doc.total_manfacture
        }
        if(!isNaN(cur_frm.doc.total_installation)){
            totalCost += cur_frm.doc.total_installation
        }
        if(!isNaN(cur_frm.doc.total_labor_cost_and_indirect_costs)){
            totalCost += cur_frm.doc.total_labor_cost_and_indirect_costs
        }
        cur_frm.set_value("total_cost", totalCost);
        refresh_field("total_cost");
    },
    total_labor_cost_and_indirect_costs:function (doc, dt, dn) {
        var totalCost = 0;
        if(!isNaN(cur_frm.doc.total_direct)){
            totalCost += cur_frm.doc.total_direct
        }
        if(!isNaN(cur_frm.doc.total_manfacture)){
            totalCost += cur_frm.doc.total_manfacture
        }
        if(!isNaN(cur_frm.doc.total_installation)){
            totalCost += cur_frm.doc.total_installation
        }
        if(!isNaN(cur_frm.doc.total_labor_cost_and_indirect_costs)){
            totalCost += cur_frm.doc.total_labor_cost_and_indirect_costs
        }
        cur_frm.set_value("total_cost", totalCost);
        refresh_field("total_cost");
    },
    total_cost:function (doc, dt, dn) {
        var grossMargin = 0;
        if(!isNaN(cur_frm.doc.total_cost) && !isNaN(cur_frm.doc.price) ){
            grossMargin = cur_frm.doc.price - cur_frm.doc.total_cost
        }
        cur_frm.set_value("gross_margin", grossMargin);
        refresh_field("gross_margin");
    },
    price:function (doc, dt, dn) {
        var grossMargin = 0;
        if(!isNaN(cur_frm.doc.total_cost) && !isNaN(cur_frm.doc.price) ){
            grossMargin = cur_frm.doc.price - cur_frm.doc.total_cost
        }
        cur_frm.set_value("gross_margin", grossMargin);
        refresh_field("gross_margin");
    },
    gross_margin:function (doc, dt, dn) {
        var grossMarginPercentage = 0;
        if(!isNaN(cur_frm.doc.gross_margin) && !isNaN(cur_frm.doc.price) ){
            grossMarginPercentage = ((cur_frm.doc.gross_margin / cur_frm.doc.price)*100).toFixed(2)+'%'
        }
        cur_frm.set_value("percentage", grossMarginPercentage);
        refresh_field("percentage");
    }
});
frappe.ui.form.on('Direct Costs', {
    item:function(frm,cdt,cdn){
        var row = locals[cdt][cdn];
        frappe.db.get_value('Item', {name: row.item}, 'stock_uom', (r) => {
			frappe.model.set_value(cdt, cdn, "uom", r.stock_uom);
		});
    },
    quantity: function (frm, cdt, cdn) {
        var row = locals[cdt][cdn];
        if (!isNaN(row.quantity) && !isNaN(row.unit_price)) {
            frappe.model.set_value(cdt, cdn, 'total', row.quantity * row.unit_price);
        }
    },
    unit_price: function (frm, cdt, cdn) {
        var row = locals[cdt][cdn];
        if (row.quantity && row.unit_price) {
            frappe.model.set_value(cdt, cdn, 'total', row.quantity * row.unit_price);
        }
    },
    total:function(frm, cdt, cdn){
        var d = locals[cdt][cdn];
        var total = 0;
        frm.doc.directcost_items.forEach(function(d) { total += d.total; });
        frm.set_value("total_direct", total);
        refresh_field("total_direct");
	},
    directcost_items_remove:function(frm, cdt, cdn){
        var d = locals[cdt][cdn];
        var total = 0;
        frm.doc.directcost_items.forEach(function(d) { total += d.total; });
        frm.set_value("total_direct", total);
        refresh_field("total_direct");
	}
});
frappe.ui.form.on('Manufacturing Cost and other outside work', {
    quantity: function (frm, cdt, cdn) {
        var row = locals[cdt][cdn];
        if (row.quantity && row.unit_price) {
            frappe.model.set_value(cdt, cdn, 'total', row.quantity * row.unit_price);
        }
    },
    unit_price: function (frm, cdt, cdn) {
        var row = locals[cdt][cdn];
        if (row.quantity && row.unit_price) {
            frappe.model.set_value(cdt, cdn, 'total', row.quantity * row.unit_price);
        }
    },
    total:function(frm, cdt, cdn){
        var d = locals[cdt][cdn];
        var total = 0;
        frm.doc.manfacture_item.forEach(function(d) { total += d.total; });
        frm.set_value("total_manfacture", total);
        refresh_field("total_manfacture");
	},
    manfacture_item_remove:function(frm, cdt, cdn){
        var d = locals[cdt][cdn];
        var total = 0;
        frm.doc.manfacture_item.forEach(function(d) { total += d.total; });
        frm.set_value("total_manfacture", total);
        refresh_field("total_manfacture");
	}
});
frappe.ui.form.on('Installation Costs', {
    quantity: function (frm, cdt, cdn) {
        var row = locals[cdt][cdn];
        if (row.quantity && row.unit_price) {
            frappe.model.set_value(cdt, cdn, 'total', row.quantity * row.unit_price);
        }
    },
    unit_price: function (frm, cdt, cdn) {
        var row = locals[cdt][cdn];
        if (row.quantity && row.unit_price) {
            frappe.model.set_value(cdt, cdn, 'total', row.quantity * row.unit_price);
        }
    },
    total:function(frm, cdt, cdn){
        var d = locals[cdt][cdn];
        var total = 0;
        frm.doc.installation_item.forEach(function(d) { total += d.total; });
        frm.set_value("total_installation", total);
        refresh_field("total_installation");
	},
    installation_item_remove:function(frm, cdt, cdn){
        var d = locals[cdt][cdn];
        var total = 0;
        frm.doc.installation_item.forEach(function(d) { total += d.total; });
        frm.set_value("total_installation", total);
        refresh_field("total_installation");
	}
});
frappe.ui.form.on('Labor Cost and Indirect Costs', {
    quantity: function (frm, cdt, cdn) {
        var row = locals[cdt][cdn];
        if (row.quantity && row.unit_price) {
            frappe.model.set_value(cdt, cdn, 'total', row.quantity * row.unit_price);
        }
    },
    unit_price: function (frm, cdt, cdn) {
        var row = locals[cdt][cdn];
        if (row.quantity && row.unit_price) {
            frappe.model.set_value(cdt, cdn, 'total', row.quantity * row.unit_price);
        }
    },
    total:function(frm, cdt, cdn){
        var d = locals[cdt][cdn];
        var total = 0;
        frm.doc.labor_and_indirect_costs_items.forEach(function(d) { total += d.total; });
        frm.set_value("total_labor_cost_and_indirect_costs", total);
        refresh_field("total_labor_cost_and_indirect_costs");
	},
    labor_and_indirect_costs_items_remove:function(frm, cdt, cdn){
        var d = locals[cdt][cdn];
        var total = 0;
        frm.doc.labor_and_indirect_costs_items.forEach(function(d) { total += d.total; });
        frm.set_value("total_labor_cost_and_indirect_costs", total);
        refresh_field("total_labor_cost_and_indirect_costs");
	}
});
