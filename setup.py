from setuptools import setup, find_packages

with open("requirements.txt") as f:
	install_requires = f.read().strip().split("\n")

# get version from __version__ variable in cost_chart/__init__.py
from cost_chart import __version__ as version

setup(
	name="cost_chart",
	version=version,
	description="cost chart",
	author="ةشقهشة",
	author_email="eng.mariam87@gmail.com",
	packages=find_packages(),
	zip_safe=False,
	include_package_data=True,
	install_requires=install_requires
)
